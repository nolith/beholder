package analyzers

import (
	"gitlab.com/i-tre-brutti/beholder/interfaces"
	"gitlab.com/i-tre-brutti/beholder/plugins"
)

// SourceAnalyzer analyzes the configured source repository, selecting the correct source plugin
func SourceAnalyzer(src interfaces.SrcConf, status interfaces.SrcData) (interfaces.SrcData, string) {
	var plugin plugins.SrcPlugin
	var newStatus interfaces.SrcData
	var lastCommitID string
	switch src.RepoType {
	case "gitlab":
		plugin = new(plugins.SrcGitlabPlugin)
		plugin.Init(src.Conf, status)
		newStatus, lastCommitID = plugin.Fetch()
		// case "github":
		// 	plugin = new(plugins.SrcGithubPlugin)
		// 	plugin.Init(src.Conf)
	}
	return newStatus, lastCommitID
}

package analyzers

import (
	"time"

	"gitlab.com/i-tre-brutti/beholder/db"
	"gopkg.in/mgo.v2/bson"

	"gitlab.com/i-tre-brutti/beholder/interfaces"
)

// ProjectAnalyzer makes the analysis of a project
func ProjectAnalyzer(conf interfaces.ProjectConf) interfaces.ProjectStatus {
	// Final status after analysis
	var results interfaces.ProjectStatus

	// Last saved status of the project
	var currentStatus interfaces.ProjectStatus
	var lastCommitID []interfaces.LastCommitsDesc

	collection := db.GetStatusCollection()
	statusErr := collection.Find(bson.M{"project_id": conf.ProjectID, "document_version": interfaces.ProjectStatusVersion}).One(&currentStatus)
	if statusErr != nil && statusErr.Error() != "not found" {
		panic(statusErr)
	}

	var sources []interfaces.SrcData
	for _, src := range conf.Sources {
		// TODO: lanciare una goroutine per ogni src
		var lastCommit interfaces.LastCommitsDesc
		res, commitID := SourceAnalyzer(src, currentStatus.Src)
		lastCommit.ProjectID = src.Conf.ProjectID
		lastCommit.CommitID = commitID
		lastCommitID = append(lastCommitID, lastCommit)
		sources = append(sources, res)
	}

	// TODO: sources deve essere mergiato in un unico risultato di tipo SrcData
	// var finalSrcResult interfaces.SrcData
	// TODO: rimuovere dopo che e' stato implementato il merge
	results.Src = sources[0]

	// TODO: il risultato mergiato deve essere inserito nella configurazione di base

	results.DocumentVersion = interfaces.ProjectStatusVersion
	results.ProjectID = conf.ProjectID
	// Formats at https://golang.org/pkg/time/#pkg-constants
	results.LastRunInfo.CommitID = lastCommitID
	results.LastRunInfo.Timestamp = time.Now().Format(time.RFC3339Nano)
	return results
}

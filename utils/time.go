package utils

import (
	"time"
)

// CompareTimestamps returns true if timestamp a is more recent than b
func CompareTimestamps(a, b string) bool {
	t1, e1 := time.Parse(time.RFC3339Nano, a)
	t2, e2 := time.Parse(time.RFC3339Nano, b)

	if e1 != nil {
		panic(e1)
	}

	if e2 != nil {
		panic(e2)
	}

	return t1.After(t2)
}

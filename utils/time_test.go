package utils

import (
	"testing"
	"time"
)

func TestCompareTimestamps(t *testing.T) {
	t1 := time.Now().Format(time.RFC3339Nano)
	time.Sleep(100 * time.Millisecond)
	t2 := time.Now().Format(time.RFC3339Nano)

	if !CompareTimestamps(t2, t1) {
		t.Error("Wrong comparison between timestamps, t2 is newer")
	}

	if CompareTimestamps(t1, t2) {
		t.Error("Wrong comparison between timestamps, t1 is older")
	}
}

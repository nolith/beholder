package utils

import "testing"

func TestRand(t *testing.T) {
	randString := RandString(10)

	if len(randString) != 10 {
		t.Error("Cannot retrieve a proper random string")
	}
}

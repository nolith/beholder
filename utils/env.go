package utils

import "os"

// IsTestingEnv return whether the code is running in testing environment
func IsTestingEnv() bool {
	env := os.Getenv("ENV")
	return env == "test"
}

package utils

import (
	"fmt"
	"net/http"
)

// MakeAPICall makes a call to the provided url and returns the response
func MakeAPICall(url string) *http.Response {
	resp, err := http.Get(url)
	if err != nil {
		fmt.Printf("SrcGitlabPlugin.fetchCommits@%s: getting url\n%s", url, err)
		panic(err)
	}
	return resp
}

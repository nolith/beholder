package db

import (
	"log"

	"gitlab.com/i-tre-brutti/beholder/interfaces"
	"gitlab.com/i-tre-brutti/beholder/utils"
	mgo "gopkg.in/mgo.v2"
)

var session *mgo.Session
var conf interfaces.GlobalConf

func initGlobalConf() {

	db := "beholder"
	if utils.IsTestingEnv() {
		db = db + "-test"
	}

	conf = interfaces.GlobalConf{
		Host:             "localhost",
		Db:               db,
		ConfCollection:   "confs",
		StatusCollection: "statuses",
	}
}

// InitSession makes the connection to the MongoDB server and saves the session
func InitSession() {
	initGlobalConf()
	log.Println("Initializing connection to", conf.Host)
	s, err := mgo.Dial(conf.Host)
	if err != nil {
		panic(err)
	}
	session = s
}

// CloseSession closes the connection to the db
func CloseSession() {
	log.Println("Closing connection to", conf.Host)
	session.Close()
}

// GetConfCollection returns the collection used to store project configurations
func GetConfCollection() *mgo.Collection {
	return getDB().C(conf.ConfCollection)
}

// GetStatusCollection returns the collection used to store project statuses
func GetStatusCollection() *mgo.Collection {
	return getDB().C(conf.StatusCollection)
}

func getDB() *mgo.Database {
	return session.DB(conf.Db)
}

// func getGlobalConfFromJSON() interfaces.GlobalConf {
// 	var conf interfaces.GlobalConf
// 	pwd, _ := os.Getwd()
// 	// Load the env variable, useful to customize conf filename
// 	env := os.Getenv("ENV")
// 	confFile := pwd + "/" + env + "conf.json"
// 	fmt.Println("Loading conf from", confFile)
// 	raw, err := ioutil.ReadFile(confFile)
// 	if err != nil {
// 		fmt.Println(err.Error())
// 		os.Exit(1)
// 	}
// 	json.Unmarshal(raw, &conf)

// 	return conf
// }

package interfaces

// SrcData holds all the info of the VCS
type SrcData struct {
	Code     SrcCode   `json:"code" bson:"code"`
	Pr       SrcPr     `json:"pr" bson:"pr"`
	Branches SrcBranch `json:"branches" bson:"branches"`
	Social   SrcSocial `json:"social" bson:"social"`
}

// SrcCode describes the code and the commits
type SrcCode struct {
	CommitsPerMonth []int          `json:"commits_per_month" bson:"commits_per_month"`
	CodeSizePerFile []RepoFileSize `json:"code_size_per_file" bson:"code_size_per_file"`
	TotalCodeSize   int            `json:"total_code_size" bson:"total_code_size"`
	FirstCommit     string         `json:"first_commit" bson:"first_commit"`
	LastCommit      string         `json:"last_commit" bson:"last_commit"`
}

// SrcPr Pull-requests info
type SrcPr struct {
	Total    int `json:"total" bson:"total"`
	Opened   int `json:"opened" bson:"opened"`
	Closed   int `json:"closed" bson:"closed"`
	Approved int `json:"approved" bson:"approved"`
	Rejected int `json:"rejected" bson:"rejected"`
}

// SrcBranch Info about branches
type SrcBranch struct {
	OpenAbandoned int `json:"open_abandoned" bson:"open_abandoned"`
	OpenWip       int `json:"open_wip" bson:"open_wip"`
	Merged        int `json:"merged" bson:"merged"`
}

// SrcSocial holds info about social aspect of the repo
type SrcSocial struct {
	Stars        int `json:"stars" bson:"stars"`
	Watchers     int `json:"watchers" bson:"watchers"`
	Forks        int `json:"forks" bson:"forks"`
	Contributors int `json:"contributors" bson:"contributors"`
}

// RepoFileSize holds the path of a file in the repo and its size
type RepoFileSize struct {
	Path string `json:"path" bson:"path"`
	Size int    `json:"size" bson:"size"`
}

package main

import (
	"log"

	"gitlab.com/i-tre-brutti/beholder/db"

	"gitlab.com/i-tre-brutti/beholder/analyzers"
	"gitlab.com/i-tre-brutti/beholder/interfaces"
	"gopkg.in/mgo.v2/bson"
)

func main() {
	// Manage database connection
	db.InitSession()
	defer db.CloseSession()

	ch := make(chan interfaces.ProjectStatus)
	confs := getProjectsConfs()

	for _, conf := range confs {
		log.Println("Spawning analyzer for project", conf.ProjectID)
		// TODO: caricare da db l'eventuale status esistente da aggiornare
		go func(c interfaces.ProjectConf) { ch <- analyzers.ProjectAnalyzer(c) }(conf)
	}

	for i := 0; i < len(confs); i++ {
		result := <-ch
		// TODO: Spostare in una goroutine
		saveProjectStatus(result)
	}
}

func saveProjectStatus(status interfaces.ProjectStatus) {
	collection := db.GetStatusCollection()
	var currentStatus interfaces.ProjectStatus

	// Find the current status of the project in the db, if it exists
	statusErr := collection.Find(bson.M{"project_id": status.ProjectID, "document_version": status.DocumentVersion}).One(&currentStatus)

	if statusErr != nil {
		// The status doesn't exist, create it
		if statusErr.Error() == "not found" {
			log.Println("Creating new status for project", status.ProjectID)
			insertError := collection.Insert(status)
			if insertError != nil {
				panic(insertError)
			}
			return
		}
		panic(statusErr)
	}

	// The status exists, update it
	log.Println("Updating status for project", status.ProjectID)
	updateErr := collection.Update(bson.M{"project_id": currentStatus.ProjectID, "document_version": status.DocumentVersion}, status)
	if updateErr != nil {
		panic(updateErr)
	}
}

func getProjectsConfs() []interfaces.ProjectConf {
	var result []interfaces.ProjectConf

	// TODO: Sostituire All con Iter
	err := db.GetConfCollection().Find(bson.M{}).All(&result)
	if err != nil {
		panic(err)
	}
	return result
}

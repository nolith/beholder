package plugins

import "gitlab.com/i-tre-brutti/beholder/interfaces"

type SrcPlugin interface {
	Init(conf interfaces.SrcRepoConf, status interfaces.SrcData)
	Fetch() (interfaces.SrcData, string)
}

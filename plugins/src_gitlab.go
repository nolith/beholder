package plugins

import (
	"encoding/json"
	"fmt"
	"log"
	"net/url"
	"sync"
	"time"

	"gitlab.com/i-tre-brutti/beholder/utils"

	"gitlab.com/i-tre-brutti/beholder/interfaces"
)

// The format is ISO8601 but I got weird errors trying to parse the true format.
// RFC3339 seems to be quite identical and it works
// https://stackoverflow.com/questions/38596079/how-do-i-parse-an-iso-8601-timestamp-in-golang
const gitlabDateFormat = time.RFC3339

// SrcGitlabPlugin holds the project configuration and the last saved status
type SrcGitlabPlugin struct {
	conf   interfaces.SrcRepoConf
	status interfaces.SrcData
}

// Init initialize the plugin setting the configuration and the previous status
func (v *SrcGitlabPlugin) Init(conf interfaces.SrcRepoConf, status interfaces.SrcData) {
	v.conf = conf
	v.status = status
}

// Fetch gets info from APIs and populates the new status
func (v *SrcGitlabPlugin) Fetch() (interfaces.SrcData, string) {
	var lastCommitID string
	project := url.PathEscape(v.conf.ProjectID)
	var newStatus interfaces.SrcData
	// newStatus = v.status
	var waitGroup sync.WaitGroup
	waitGroup.Add(2)
	go func() {
		defer waitGroup.Done()
		newStatus.Code, lastCommitID = fetchCodeInfo(project)
	}()
	go func() {
		defer waitGroup.Done()
		newStatus.Branches = fetchBranchesInfo(project)
	}()
	waitGroup.Wait()
	return newStatus, lastCommitID
}

func fetchCodeInfo(project string) (interfaces.SrcCode, string) {
	log.Println("Fetching code info about project", project)
	var result interfaces.SrcCode
	var waitGroup sync.WaitGroup

	waitGroup.Add(2)
	var lastCommitID string
	go func() {
		defer waitGroup.Done()
		lastCommitID = fetchCommits(project, &result)
	}()

	go func() {
		defer waitGroup.Done()
		fetchCodeSize(project, &result)
	}()

	waitGroup.Wait()

	return result, lastCommitID
}

func fetchBranchesInfo(project string) interfaces.SrcBranch {
	log.Println("Fetching code info about project", project)
	var result interfaces.SrcBranch
	var branches []branch
	url := fmt.Sprintf("https://gitlab.com/api/v4/projects/%s/repository/branches", project)
	resp := utils.MakeAPICall(url)
	err := json.NewDecoder(resp.Body).Decode(&branches)
	defer resp.Body.Close()
	if err != nil {
		log.Printf("SrcGitlabPlugin.fetchCodeLines@%s: reading response\n%s", url, err)
		panic(err)
	}

	for _, b := range branches {
		log.Println("Analyzing branch", b.Name)
		if b.Merged {
			result.Merged++
		} else {
			// Open branches with the last commit older than 1 month is considered "abandoned"
			oldDate := time.Now().AddDate(0, -1, 0)
			commitDate, err := time.Parse(gitlabDateFormat, b.Commit.CommittedDate)
			if err != nil {
				log.Println("Date parse error", b.Commit.CommittedDate)
				panic(err)
			}
			if commitDate.Before(oldDate) {
				result.OpenAbandoned++
			} else {
				result.OpenWip++
			}
		}
	}

	return result
}

func inSameMonth(c1 *commit, c2 *commit) (bool, error) {
	//const gitlabDateFormat = "2006-01-02"
	t1, err := time.Parse(gitlabDateFormat, c1.CreatedAt)
	if err != nil {
		log.Printf("Date parse error %v", c1.CreatedAt)
		return false, err
	}
	y1, m1, _ := t1.Date()
	t2, err := time.Parse(gitlabDateFormat, c2.CreatedAt)
	if err != nil {
		log.Printf("Date parse error %v", c2.CreatedAt)
		return false, err
	}
	y2, m2, _ := t2.Date()
	return y2 == y1 && m2 == m1, nil
}

func fetchCodeSize(project string, result *interfaces.SrcCode) {
	log.Println("Fetching code size")

	var files []repoFile

	url := fmt.Sprintf("https://gitlab.com/api/v4/projects/%s/repository/tree?recursive=1", project)
	// If the call takes too long, we should switch to use the `path` param instead of `recursive`
	resp := utils.MakeAPICall(url)
	err := json.NewDecoder(resp.Body).Decode(&files)
	defer resp.Body.Close()
	if err != nil {
		log.Printf("SrcGitlabPlugin.fetchCodeLines@%s: reading response\n%s", url, err)
		panic(err)
	}

	var sizePerFile []interfaces.RepoFileSize
	ch := make(chan interfaces.RepoFileSize)

	var waitGroup sync.WaitGroup
	waitGroup.Add(len(files))
	for _, file := range files {
		go func(p, filePath, fileType string) {
			defer waitGroup.Done()
			if fileType == "blob" {
				ch <- getFileSize(p, filePath)
			}
		}(project, file.Path, file.Type)
	}

	totalSize := 0
	go func() {
		for result := range ch {
			totalSize = totalSize + result.Size
			sizePerFile = append(sizePerFile, interfaces.RepoFileSize{Path: result.Path, Size: result.Size})
		}
	}()
	waitGroup.Wait()
	close(ch)

	result.CodeSizePerFile = sizePerFile
	result.TotalCodeSize = totalSize
}

func getFileSize(project, filePath string) interfaces.RepoFileSize {
	log.Println("Fetching size for file", filePath)
	var file repoFileDetails
	escapedFilePath := url.PathEscape(filePath)
	url := fmt.Sprintf("https://gitlab.com/api/v4/projects/%s/repository/files/%s?ref=master", project, escapedFilePath)
	resp := utils.MakeAPICall(url)
	err := json.NewDecoder(resp.Body).Decode(&file)
	defer resp.Body.Close()
	if err != nil {
		log.Printf("SrcGitlabPlugin.fetchCodeLines@%s: reading response\n%s", url, err)
		panic(err)
	}
	return interfaces.RepoFileSize{Path: filePath, Size: file.Size}
}

func fetchCommits(project string, result *interfaces.SrcCode) string {
	log.Println("Fetching commits")

	url := fmt.Sprintf("https://gitlab.com/api/v4/projects/%s/repository/commits", project)
	resp := utils.MakeAPICall(url)

	var commits []commit
	var lastCommitID string
	err := json.NewDecoder(resp.Body).Decode(&commits)
	defer resp.Body.Close()
	if err != nil {
		log.Printf("SrcGitlabPlugin.fetchCommits@%s: reading response\n%s", url, err)
		panic(err)
	}

	if len(commits) > 0 {
		lastCommitID = commits[0].ID
	}
	result.CommitsPerMonth = calculateCommitsPerMonth(commits)
	result.FirstCommit = commits[len(commits)-1].CreatedAt
	result.LastCommit = commits[0].CreatedAt

	return lastCommitID
}

func calculateCommitsPerMonth(commits []commit) []int {
	var commitsPerMonth = []int{}
	current, firstOfSequence := 0, 0
	for ; current < len(commits); current++ {
		res, err := inSameMonth(&commits[current], &commits[firstOfSequence])
		if err != nil {
			log.Printf("ERROR %vi\n", err)
			continue
		}
		if !res {
			commitsPerMonth = append(commitsPerMonth, current-firstOfSequence)
			firstOfSequence = current
		}
	}
	// No more commits to check, let's simulate a change of month to append the remained commits
	if len(commits) > 0 {
		commitsPerMonth = append(commitsPerMonth, current-firstOfSequence)
	}

	return commitsPerMonth
}

type commit struct {
	ID             string   `json:"id"`
	ShortID        string   `json:"short_id"`
	Title          string   `json:"title"`
	CreatedAt      string   `json:"created_at"`
	ParentIds      []string `json:"parent_ids"`
	Message        string   `json:"message"`
	AuthorName     string   `json:"author_name"`
	AuthorEmail    string   `json:"author_email"`
	AuthoredDate   string   `json:"authored_date"`
	CommitterName  string   `json:"committer_name"`
	CommitterEmail string   `json:"committer_email"`
	CommittedDate  string   `json:"committed_date"`
}

type branch struct {
	Name   string `json:"name"`
	Commit commit `json:"commit"`
	Merged bool   `json:"merged"`
}

type repoFile struct {
	ID   string `json:"id"`
	Name string `json:"name"`
	Type string `json:"type"`
	Path string `json:"path"`
	Mode string `json:"mode"`
}

type repoFileDetails struct {
	FileName     string `json:"file_name"`
	FilePath     string `json:"file_path"`
	Size         int    `json:"size"`
	Encoding     string `json:"encoding"`
	Content      string `json:"content"`
	Ref          string `json:"ref"`
	BlobID       string `json:"blob_id"`
	CommitID     string `json:"commit_id"`
	LastCommitID string `json:"last_commit_id"`
}

func (c *commit) String() string {
	return fmt.Sprintf("[%s:%s] <%s @%s> %s", c.ShortID, c.CreatedAt, c.AuthorName, c.AuthorEmail, c.Title)
}

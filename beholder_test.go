package main

// func TestMain(m *testing.M) {
// 	os.Setenv("ENV", "test")
// 	db.InitSession()
// 	defer db.CloseSession()
// 	os.Exit(m.Run())
// }

// func TestSaveProjectStatus(t *testing.T) {
// 	collection := db.GetStatusCollection()

// 	// Delete all existing statuses
// 	collection.RemoveAll(nil)

// 	id := "test_" + utils.RandString(6)
// 	status := fixtures.GetEmptyProjectStatus(id)
// 	saveProjectStatus(status)

// 	// check the status has been created
// 	var createdStatus interfaces.ProjectStatus
// 	stErr := collection.Find(bson.M{"project_id": id}).One(&createdStatus)
// 	if stErr != nil {
// 		t.Error("Error creating new status:", stErr)
// 	}
// 	// Save current timestamp
// 	t1 := createdStatus.LastRunInfo.Timestamp

// 	// Update the timestamp of the status and call the function
// 	fixtures.UpdateProjectStatusTimestamp(&createdStatus)
// 	saveProjectStatus(createdStatus)

// 	// Now check the timestamp in the db changed, this means the doc
// 	// has been updated
// 	var updatedStatus interfaces.ProjectStatus
// 	stErr2 := collection.Find(bson.M{"project_id": id}).One(&updatedStatus)
// 	if stErr2 != nil {
// 		t.Error("Error updating the status:", stErr2)
// 	}
// 	t2 := updatedStatus.LastRunInfo.Timestamp

// 	if !utils.CompareTimestamps(t2, t1) {
// 		t.Error("Error comparing the statuses", t1, t2)
// 	}
// }

// func TestGetProjectConfs(t *testing.T) {
// 	confs := getProjectsConfs()

// 	if len(confs) != 2 {
// 		t.Error("Error loading projects configuration", len(confs))
// 	}
// }

package fixtures

import (
	"time"

	"gitlab.com/i-tre-brutti/beholder/interfaces"
)

func GetEmptyProjectStatus(projectID string) interfaces.ProjectStatus {
	status := interfaces.ProjectStatus{ProjectID: projectID}
	status.LastRunInfo.Timestamp = getTimestamp()

	return status
}

func UpdateProjectStatusTimestamp(status *interfaces.ProjectStatus) {
	status.LastRunInfo.Timestamp = getTimestamp()
}

func getTimestamp() string {
	return time.Now().Format(time.RFC3339Nano)
}
